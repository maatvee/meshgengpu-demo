# MeshGenGPU - Unity Demo

Naive Surface Nets: Procedural Mesh - Generated on the GPU using compute shaders

Requires Unity Build: **2020.3.33f1** and uses the URP (Universal Render Pipeline)

<img src="MesGenGPU_demo.png" width="400" />

https://www.youtube.com/watch?v=CXSZftNFQ58

## Project Summary
The original Unity implementation by Tomasz Foster, generated a procedural mesh using a standard C# script.
Unfortunately the performance was severely lacking for any useful real-time surface creation.

I've re-written the implementation by using a few compute shaders, which offload all the mesh generation to the GPU.

The first compute shader calculates all the vertex information for the isosurface based on the proximity of the various field objects in the scene, and writes the vertices to a buffer.
The second compute shader reads the vertex information from the buffer and then stitches them into a surface mesh by creating triangles that make up each quad.

The algorithm basically generates 1-3 quads for every voxel that contains surface vertices.

#### Quad Vertex Layout ABCD

<img src="quad-layout.png" width="200" />

#### Quad Vertex positions
* Tri A = ACD
* Tri B = ABC

There is a great article that explains how the naive surface nets algorithm works here:

https://bonsairobo.medium.com/smooth-voxel-mapping-a-technical-deep-dive-on-real-time-surface-nets-and-texturing-ef06d0f8ca14

## Installation Instructions

```git clone https://gitlab.com/maatvee/meshgengpu-demo.git```


## References
Unity Implementation extending the work of TomaszFoster
https://github.com/TomaszFoster/NaiveSurfaceNets

I should add that I found TomaszFoster's implementation in the comments of this page: https://0fps.net/2012/07/12/smooth-voxel-terrain-part-2/

So the credit should really go to mikolalysenko, who wrote the gist of this implementation in JavaScript.
https://github.com/mikolalysenko/mikolalysenko.github.com/blob/master/Isosurface/js/surfacenets.js

Demo here: https://mikolalysenko.github.io/Isosurface/

### Uses these Unity Assets

https://assetstore.unity.com/packages/vfx/shaders/wireframe-shader-the-amazing-wireframe-shader-18794
https://assetstore.unity.com/packages/tools/utilities/surge-107312

