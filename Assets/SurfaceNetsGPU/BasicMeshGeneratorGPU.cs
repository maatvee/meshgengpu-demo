using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEditor;
using UnityEngine.Rendering;

using SurfaceNetsGPU;
using Pixelplacement;
using Random = UnityEngine.Random;

public class BasicMeshGeneratorGPU : MonoBehaviour {
    
    private int voxelBufferSize;
    private const int MAXDim = 32;
    private const int MAXOffset = 64;
    
    [Header("Sample Field Settings")]
    [SerializeField] [Range(0, 100)] private float threshold = 0.3f;
    [SerializeField] [Range (0,5)] private float samplesPerMeter = 1.0f;
    [SerializeField] private Boolean scaleFieldWithResolution = true;
    [Range(1,MAXDim)] public int xSize = 8;
    [Range(1,MAXDim)] public int ySize = 8;
    [Range(1,MAXDim)] public int zSize = 8;
    [SerializeField] private int sampleWidth;
    [SerializeField] private int sampleHeight;
    [SerializeField] private int sampleDepth;
    
    [Range(-MAXOffset,MAXOffset)] public int xPositionOffset = 0;
    [Range(-MAXOffset,MAXOffset)] public int yPositionOffset = 0;
    [Range(-MAXOffset,MAXOffset)] public int zPositionOffset = 0;
    
    [Header("Mesh Settings")]
    [Space(20)]
    [SerializeField] private Boolean subdivideMesh = false;
    
    [Header("Field Object Settings")]
    [Space(20)]
    [SerializeField] private Boolean generateRandomFieldObjects = false;
    [SerializeField] private fieldObjectType FieldObjectType;
    const int minIdx = 0;
    const int maxIdx = 511;
    
    [SerializeField] private int numberOfFieldObjects = 20;
    [SerializeField] private GameObject pointObject;
    [SerializeField] private GameObject sphereObject;
    [SerializeField] private GameObject boxObject;
    [SerializeField] [Range(1, 50)] private int numberOfSplinePoints = 20;

    [SerializeField] private GameObject fieldObjectsParent;
    [SerializeField] private GameObject[] fieldObjects;
    [SerializeField] private Spline fieldSpline;
    [SerializeField] private GameObject splineTarget;
    private Vector3[] fieldObjectPositions;
    [SerializeField] private FieldObject[] fieldObjectData;

    public enum fieldObjectType {
        Point,
        Sphere,
        Box
    }
    
    [Range(minIdx,maxIdx)] private int inspectIndex;
    private Vector3 inspectPos;
    private Boolean inspectIndexEnabled = false;
    [SerializeField] private Voxel[] voxels;
    
    private GameObject vertClone;
    private GameObject pointClone;
    private GameObject splineTargetClone;
    
    [Header("Compute Shader Settings")]
    [Space(20)]
    
    [SerializeField] private int numTriangles;
    
    
    [SerializeField] Material meshMaterial;
    
    private Material _meshMaterial;

    [SerializeField] private ComputeShader CalcVerts;
    [SerializeField] private ComputeShader GenerateMesh;
    [SerializeField] private ComputeShader CountVerts = default;
    // private Boolean showGizmos = false;
    
    private Bounds localBounds;
    private Bounds bounds;
    // The id of the CalcVertsKernel in the main compute shader
    private int CalcVertsKernel;
    // The id of the GenerateMeshKernel in the main compute shader
    private int GenerateMeshKernel;
    // The id of the CountVertKernel in the tri to vert count compute shader
    private int CountVertsKernel;
    // A state variable to help keep track of whether compute buffers have been set up
    private bool initialized;
    // A compute buffer to hold voxel data of the source mesh
    private ComputeBuffer voxelDataBuffer;
    // A compute buffer to hold vertex data of the generated mesh
    private ComputeBuffer drawBuffer;
    // A compute buffer to hold indirect draw arguments
    private ComputeBuffer argsBuffer;
    // A compute buffer to store the pre-computed edgeTable array.
    private ComputeBuffer edgeTableBuffer;
    // A compute buffer to store the pre-computed cubeEdges array.
    private ComputeBuffer cubeEdgesBuffer;
    // A compute buffer to store the pre-computed voxelCornerOffsets array.
    private ComputeBuffer voxelCornerOffsetsBuffer;
    // A compute buffer to store the fieldObjects array.
    private ComputeBuffer fieldObjectsBuffer;
    private ComputeBuffer voxDataLen;
    
    // Define the edge table and cube edges with a set size
    private int[] edgeTable = new int[256];
    private int[] cubeEdges = new int[24];
    
    [Header("Compute Shader Dispatch Settings")]
    [Space(20)]
    // The x,y,z dispatch sizes for the compute shader
    [SerializeField] private int dispatchSizeX;
    [SerializeField] private int dispatchSizeY;
    [SerializeField] private int dispatchSizeZ;
    
    [SerializeField] private int dispatchSizeXGenMesh;
    [SerializeField] private int dispatchSizeYGenMesh;
    [SerializeField] private int dispatchSizeZGenMesh;
    // The size of one entry into the various compute buffers

    private const int INT_STRIDE = sizeof(int);
    private const int VEC3_STRIDE = sizeof(float) * 3;
    private const int VEC4_STRIDE = sizeof(float) * 4;
    
    private const int VOXEL_STRIDE = sizeof(int) * 4 + sizeof(float) * 3;
    
    // DRAW_STRIDE Size Logic
    // sizeof(float) * 3 + ((3 + 4) * 3);
    // 3 for the normal of each triangle - not used
    // 4 for the world space position of each vertex (x3) of each triangle
    // 3 for the UV3 that is calculated for each vertex for each triangle
    //private const int DRAW_STRIDE = sizeof(float) * 3 + ((3 + 4) * 3); //this may need to change based on how we draw the triangles
    
    private const int DRAW_STRIDE = sizeof(float) * ((3 + 4) * 3); //removed normal
    private const int ARGS_STRIDE = sizeof(int) * 4;
    private const int FIELD_OBJECT_STRIDE = sizeof(int) + sizeof(float) * 7;

    [Serializable] 
    public struct Voxel {
        public int vertexIndex;
        public Vector3 vertexPosition;
        public int isOnSurface; // using int because bool is not a blittable data type
        public int voxelEdgeMask;
        public int cornerMask;
    }

    [Serializable]
    public struct FieldObject {
        public int type; // 0 - point, 1 sphere, 2 box, 3 spline
        public Vector3 pos;
        public float radius;
        public Vector3 boxSize;
    } 
    private void OnDrawGizmos() {
        sampleWidth = xSize * 8;
        sampleHeight = ySize * 8;
        sampleDepth = zSize * 8;
        
        Gizmos.color = Color.yellow;
        var size = new Vector3(sampleWidth/samplesPerMeter, sampleHeight/samplesPerMeter, sampleDepth/samplesPerMeter);
        //var center = new Vector3(xPositionOffset,yPositionOffset,zPositionOffset);
        var center = Vector3.zero;
        //Gizmos.DrawWireCube(localBounds.center, localBounds.size);
        Gizmos.DrawWireCube(center, size);
    }
    
    void Awake() {
        
        // ensures that the view stays in the editor view on Play;
        //UnityEditor.SceneView.FocusWindowIfItsOpen(typeof(UnityEditor.SceneView));
        
        sampleWidth = xSize * 8;
        sampleHeight = ySize * 8;
        sampleDepth = zSize * 8;
        
        // precompute lookup tables to be passed to the GPU in their respective buffers
        cubeEdges = MeshUtilsGPU.GenerateCubeEdgesTable();
        edgeTable = MeshUtilsGPU.GenerateIntersectionTable(cubeEdges);

        if (generateRandomFieldObjects) {
            //threshold = 0.9f; // here we set the threshold to a more reasonable value for this demo
            RandomFieldObjectsGenerator(numberOfFieldObjects);            
        }

        // create fieldObjects from spline
        if (!generateRandomFieldObjects && fieldSpline != null && splineTarget != null) {
            generatePointsFromSpline(numberOfSplinePoints);
        }
    }


    // Start is called before the first frame update
    void OnEnable() {
        
        // If initialized, call on disable to clean things up
        if(initialized) {
            OnDisable();
        }
        initialized = true;
        
        CalcVertsKernel = CalcVerts.FindKernel("CalcVertsGPU");
        GenerateMeshKernel = GenerateMesh.FindKernel("GenerateMeshGPU");
        CountVertsKernel = CountVerts.FindKernel("CountVertsGPU");
        
        numTriangles = sampleWidth * sampleHeight * sampleDepth * 3 * 2; // number of triangle vertices

        voxelBufferSize = sampleWidth * sampleHeight * sampleDepth;
        
        // Clone the given material before using.
        _meshMaterial = new Material(meshMaterial);
        _meshMaterial.name += " (cloned)";
        int maxFieldObjects = 128;
        // initialise draw buffer size
        drawBuffer = new ComputeBuffer(numTriangles, DRAW_STRIDE, ComputeBufferType.Append);
        argsBuffer = new ComputeBuffer(1, ARGS_STRIDE, ComputeBufferType.IndirectArguments);
        
        edgeTableBuffer = new ComputeBuffer(256, INT_STRIDE, ComputeBufferType.Structured);
        cubeEdgesBuffer = new ComputeBuffer(24, INT_STRIDE, ComputeBufferType.Structured);
        voxelCornerOffsetsBuffer = new ComputeBuffer(8, VEC3_STRIDE, ComputeBufferType.Structured);
        
        fieldObjectsBuffer = new ComputeBuffer(maxFieldObjects, FIELD_OBJECT_STRIDE, ComputeBufferType.Structured);
        
        voxelDataBuffer = new ComputeBuffer(voxelBufferSize,VOXEL_STRIDE,ComputeBufferType.Structured);
        voxDataLen = new ComputeBuffer(1, INT_STRIDE, ComputeBufferType.Counter);
        
        argsBuffer.SetData(new int[] { 0, 1, 0, 0 });
        drawBuffer.SetCounterValue(0); // Set the count to zero

        cubeEdgesBuffer.SetData(cubeEdges);
        edgeTableBuffer.SetData(edgeTable);
        voxelCornerOffsetsBuffer.SetData(MeshUtilsGPU.voxelCornerOffsets);

        // Calculate the number of threads to use. Get the thread size from the CalcVertsKernel
        // Then, divide the number of triangles by that size
        CalcVerts.GetKernelThreadGroupSizes(CalcVertsKernel, out uint threadGroupSizeX, out uint threadGroupSizeY, out uint threadGroupSizeZ );
        GenerateMesh.GetKernelThreadGroupSizes(CalcVertsKernel, out uint threadGroupSizeXGenMesh, out uint threadGroupSizeYGenMesh, out uint threadGroupSizeZGenMesh );
        
        dispatchSizeX = Mathf.CeilToInt((float) sampleWidth / threadGroupSizeX);
        dispatchSizeY = Mathf.CeilToInt((float) sampleHeight / threadGroupSizeY);
        dispatchSizeZ = Mathf.CeilToInt((float) sampleDepth / threadGroupSizeZ);

        dispatchSizeXGenMesh = Mathf.CeilToInt((float) sampleWidth / threadGroupSizeXGenMesh);
        dispatchSizeYGenMesh = Mathf.CeilToInt((float) sampleHeight / threadGroupSizeYGenMesh);
        dispatchSizeZGenMesh = Mathf.CeilToInt((float) sampleDepth / threadGroupSizeZGenMesh);
        
        GenerateMesh.SetBuffer(GenerateMeshKernel, "_DrawTriangles", drawBuffer); //used by GenerateMesh
        
        CalcVerts.SetBuffer(CalcVertsKernel, "_EdgeTable", edgeTableBuffer); //used by both
        GenerateMesh.SetBuffer(GenerateMeshKernel, "_EdgeTable", edgeTableBuffer); //used by both
        
        CalcVerts.SetBuffer(CalcVertsKernel, "_CubeEdges", cubeEdgesBuffer); //used by CalcVerts
        CalcVerts.SetBuffer(CalcVertsKernel, "_VoxelCornerOffsets", voxelCornerOffsetsBuffer); //used by CalcVerts

        CalcVerts.SetBuffer(CalcVertsKernel, "_VoxelDataBuffer", voxelDataBuffer);  //used by both
        GenerateMesh.SetBuffer(GenerateMeshKernel, "_VoxelDataBuffer", voxelDataBuffer);  //used by both
        
        CalcVerts.SetBuffer(CalcVertsKernel, "_FieldObjects", fieldObjectsBuffer); //used by CalcVerts
        CalcVerts.SetInt("_FieldObjectsLength", fieldObjectData.Length); //used by CalcVerts

        _meshMaterial.SetBuffer("_DrawTriangles", drawBuffer);
        
        CountVerts.SetBuffer(CountVertsKernel, "_IndirectArgsBuffer", argsBuffer);
    }
    
    private void OnDisable() {
        // Dispose of buffers
        if(initialized) {
            drawBuffer.Release();
            argsBuffer.Release();
            
            edgeTableBuffer.Release();
            cubeEdgesBuffer.Release();
            voxelCornerOffsetsBuffer.Release();
            fieldObjectsBuffer.Release();
            
            voxelDataBuffer.Release();
            voxDataLen.Release();
        }
        initialized = false;
    }

    // This applies the game object's transform to the local bounds
    // Code by benblo from https://answers.unity.com/questions/361275/cant-convert-bounds-from-world-coordinates-to-loca.html
    public Bounds TransformBounds(Bounds boundsOS) {
        var center = transform.TransformPoint(boundsOS.center);

        // transform the local extents' axes
        var extents = boundsOS.extents;
        var axisX = transform.TransformVector(extents.x, 0, 0);
        var axisY = transform.TransformVector(0, extents.y, 0);
        var axisZ = transform.TransformVector(0, 0, extents.z);

        // sum their absolute value to get the world extents
        extents.x = Mathf.Abs(axisX.x) + Mathf.Abs(axisY.x) + Mathf.Abs(axisZ.x);
        extents.y = Mathf.Abs(axisX.y) + Mathf.Abs(axisY.y) + Mathf.Abs(axisZ.y);
        extents.z = Mathf.Abs(axisX.z) + Mathf.Abs(axisY.z) + Mathf.Abs(axisZ.z);

        return new Bounds { center = center, extents = extents };
    }
    
    private void LateUpdate() {
        UpdateMeshGPU();
    }

    void updateSplinePoints() {
        for (int i = 0; i < fieldObjects.Length; i++) {
            
            var collider = fieldObjects[i].GetComponent<Collider>();
            if (collider.GetComponent<SphereCollider>()) {
                float step = (float) i / fieldObjects.Length;
                Vector3 pos = fieldSpline.GetPosition(step);
                fieldObjects[i].transform.position = pos;
            }
        }
    }
    
    void updateFieldObjectData(GameObject[] fieldObjects) {
        var fieldObjectDataList = new List<FieldObject>();
        for (int i = 0; i < fieldObjects.Length; i++) {
            if (fieldObjects[i] != null) {
                Vector3 pos = fieldObjects[i].transform.position;
                FieldObject pointFO = new FieldObject();
                
                pointFO.pos = pos;
                
                var collider = fieldObjects[i].GetComponent<Collider>();
                
                if (collider.GetComponent<BoxCollider>() ) {
                    pointFO.boxSize = fieldObjects[i].transform.localScale;
                    pointFO.type = 2; // box with random dimensions
                    pointFO.radius = 0;
                } else {
                    pointFO.boxSize = Vector3.zero;
                    pointFO.type = 1;
                    pointFO.radius = fieldObjects[i].transform.localScale.x / 2;
                }

                fieldObjectDataList.Add(pointFO);
            }
        }
        
        fieldObjectData = fieldObjectDataList.ToArray();
        fieldObjectsBuffer.SetData(fieldObjectData);
    }

    void generatePointsFromSpline(int numberOfPoints) {
        var splinePointsList = new List<GameObject>();
        var splinePointsDataList = new List<FieldObject>();
        
        for (int i = 0; i < fieldObjects.Length; i++) {
            var obj = fieldObjects[i];
            splinePointsList.Add(obj);
        }
        
        for (int i = 0; i < numberOfPoints; i++) {

            float step = (float) i / numberOfPoints;
            
            Vector3 pos = fieldSpline.GetPosition(step);
            
            splineTargetClone = Instantiate(splineTarget, pos, splineTarget.transform.rotation) as GameObject;
            splineTargetClone.name = $"SplineObject {i}";
            splineTargetClone.transform.parent = fieldObjectsParent.transform;
            splinePointsList.Add(splineTargetClone);

            FieldObject splineFO = new FieldObject();
            splineFO.type = 1; // sphere
            splineFO.pos = pos;
            splineFO.boxSize = Vector3.zero;
             
            var collider = splineTargetClone.GetComponent<Collider>();

            splineFO.radius = collider.GetComponent<SphereCollider>().radius;
            
            splinePointsDataList.Add(splineFO);
        }

        fieldObjects = splinePointsList.ToArray();
        fieldObjectData = splinePointsDataList.ToArray();
    }

    void RandomFieldObjectsGenerator(int randomPoints) {
        // generate random points

        var gameObjectsList = new List<GameObject>();
        var randomPointsList = new List<FieldObject>();

        for (int i = 0; i < fieldObjects.Length; i++) {
            gameObjectsList.Add(fieldObjects[i]);
        }

        for (int i = 0; i < randomPoints; i++ ) {
            // only generate random points inside the sampleSpace
            var minX = sampleWidth * 0.5f;
            var minY = sampleHeight * 0.5f;
            var minZ = sampleDepth * 0.5f;

            var minB = 2f;
            var maxB = 10f;
            Vector3 pos = new Vector3(Random.Range(-minX, minX), Random.Range(-minY, minY), Random.Range(-minZ, minZ));
            
            FieldObject pointFO = new FieldObject();
            if (FieldObjectType == fieldObjectType.Point) {
                pointFO.type = 0; // box with random dimensions
                pointFO.pos = pos;
                pointFO.boxSize = Vector3.zero;
                pointFO.radius = 0f; //Random.Range(0.1f, 3f);
                pointClone = Instantiate(pointObject, pos, pointObject.transform.rotation) as GameObject;
                pointClone.name = $"Point {i}";

            } else if (FieldObjectType == fieldObjectType.Sphere) {
                var randomRadius = Random.Range(0.02f, 3f) * 2;
                pointFO.type = 1; // box with random dimensions
                pointFO.pos = pos;
                pointFO.boxSize = Vector3.zero;
                pointFO.radius = randomRadius / 2;
                pointClone = Instantiate(sphereObject, pos, sphereObject.transform.rotation) as GameObject;
                pointClone.name = $"Sphere {i}";
                pointClone.transform.localScale = new Vector3(randomRadius,randomRadius,randomRadius);

            } else if (FieldObjectType == fieldObjectType.Box) {
                Vector3 boxSize = new Vector3(Random.Range(minB, maxB), Random.Range(minB, maxB), Random.Range(minB, maxB));
                pointFO.type = 2; // box with random dimensions
                pointFO.pos = pos;
                pointFO.boxSize = boxSize;
                pointFO.radius = 0f;
                pointClone = Instantiate(boxObject, pos, boxObject.transform.rotation) as GameObject;
                pointClone.name = $"Box {i}";
                pointClone.transform.localScale = boxSize;
            }  
            
            pointClone.transform.parent = fieldObjectsParent.transform;
            gameObjectsList.Add(pointClone);
            randomPointsList.Add(pointFO);
        }
        
        fieldObjects = gameObjectsList.ToArray();
        
        // here we extract just the vector3 positions of each field object and shove them in an array
        // that will be passed to the GPU using the fieldObjects buffer and FieldObjects Struct
        fieldObjectData = randomPointsList.ToArray();
    }
    
    void UpdateMeshGPU() {
        
        sampleWidth = xSize * 8;
        sampleHeight = ySize * 8;
        sampleDepth = zSize * 8;
      
        // Set the count to zero
        drawBuffer.SetCounterValue(0);

        var adjustedThreshold = threshold;
        
        if (scaleFieldWithResolution) {
            
            var newScale = 1 / samplesPerMeter;
            fieldObjectsParent.transform.localScale = new Vector3(newScale,newScale,newScale);
            adjustedThreshold = threshold / samplesPerMeter;
        }

        CalcVerts.SetFloat("_Threshold", adjustedThreshold);
        GenerateMesh.SetFloat("_Threshold", adjustedThreshold);
        
        CalcVerts.SetInt("_gridX", sampleWidth);
        CalcVerts.SetInt("_gridY", sampleHeight);
        CalcVerts.SetInt("_gridZ", sampleDepth);
        
        CalcVerts.SetInt("_xPos", xPositionOffset);
        CalcVerts.SetInt("_yPos", yPositionOffset);
        CalcVerts.SetInt("_zPos", zPositionOffset);
        
        GenerateMesh.SetInt("_gridX", sampleWidth);
        GenerateMesh.SetInt("_gridY", sampleHeight);
        GenerateMesh.SetInt("_gridZ", sampleDepth);
        
        GenerateMesh.SetInt("_xPos", xPositionOffset);
        GenerateMesh.SetInt("_yPos", yPositionOffset);
        GenerateMesh.SetInt("_zPos", zPositionOffset);
        
        CalcVerts.SetInt("_inspectIndex", inspectIndex);
        CalcVerts.SetBool("_inspectIndexEnabled", inspectIndexEnabled);
        
        GenerateMesh.SetInt("_inspectIndex", inspectIndex);
        GenerateMesh.SetBool("_inspectIndexEnabled", inspectIndexEnabled);
        GenerateMesh.SetBool("_subdivideMesh", subdivideMesh);
        
        CalcVerts.SetFloat("_SamplesPerMeter", samplesPerMeter); //used by CalcVerts

        var emptyVox = new Voxel[voxelBufferSize];
        
        voxelDataBuffer.SetCounterValue(0);
        
        int inX = (int) Mathf.Floor(inspectIndex % (sampleWidth - 1));
        int inY = (int) Mathf.Floor(inspectIndex/(sampleWidth - 1) % (sampleHeight-1));
        int inZ = (int) Mathf.Floor(inspectIndex / ((sampleWidth - 1) * (sampleHeight-1)));

        inspectPos = new Vector3(inX,inY,inZ);
        
        if (!generateRandomFieldObjects && fieldSpline != null && splineTarget != null) {
            updateSplinePoints();
        }

        updateFieldObjectData(fieldObjects);
        
        CalcVerts.SetInt("_FieldObjectsLength", fieldObjectData.Length); //used by CalcVerts
        
        //transform.position = new Vector3(xPositionOffset, yPositionOffset, zPositionOffset);

        CalcVerts.SetFloat("_Time",Time.time); // pass in deltaTime as the noise seed
        CalcVerts.SetMatrix("_LocalToWorld", transform.localToWorldMatrix);
        CalcVerts.SetMatrix("_WorldToLocal", transform.worldToLocalMatrix);
        CalcVerts.SetMatrix("_FO_LocalToWorld", fieldObjectsParent.transform.localToWorldMatrix);
        CalcVerts.SetMatrix("_FO_WorldToLocal", fieldObjectsParent.transform.worldToLocalMatrix);
        
        //var boundsCenter = new Vector3(xPositionOffset, yPositionOffset, zPositionOffset);
        //var boundsExtents = new Vector3(xSize,ySize,zSize);
        //var boundsExtents = new Vector3(xSize * samplesPerMeter,ySize * samplesPerMeter,zSize * samplesPerMeter);
        var boundsExtents = new Vector3(0.1f,0.1f,0.1f);
        var boundsCenter = Vector3.zero; //new Vector3(xPositionOffset, yPositionOffset, zPositionOffset);
        
        localBounds = new Bounds(boundsCenter, boundsExtents);
        
        // Transform the bounds to world space
        bounds = TransformBounds(localBounds);

        CalcVerts.Dispatch(CalcVertsKernel, dispatchSizeX,dispatchSizeY,dispatchSizeZ);
        GenerateMesh.Dispatch(GenerateMeshKernel, dispatchSizeXGenMesh,dispatchSizeYGenMesh,dispatchSizeZGenMesh);
        
        // Copy the count (stack size) of the draw buffer to the args buffer, at byte position zero
        // This sets the vertex count for our draw procedural indirect call
        ComputeBuffer.CopyCount(drawBuffer, argsBuffer, 0);
        
        // This the compute shader outputs triangles, but the graphics shader needs the number of vertices,
        // we need to multiply the vertex count by three. We'll do this on the GPU with a compute shader 
        // so we don't have to transfer data back to the CPU
        CountVerts.Dispatch(CountVertsKernel, 1, 1, 1);
        
        Graphics.DrawProceduralIndirect(
            _meshMaterial, 
            localBounds,
            MeshTopology.Triangles,
            argsBuffer, 
            0, 
            null, 
            null, 
            ShadowCastingMode.Off, 
            false, 
            gameObject.layer);
    }
}
