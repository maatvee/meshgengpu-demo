// MIT License

// Copyright (c) 2022 Mat Valdman

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files(the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and / or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions :

// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.


// Basic Unlit Shader that renders Quads as a wireframe
// The shader name and folder as displayed in the shader picker dialog in a material
Shader "Custom/WireFrameGPU" {
    Properties {
        // Shader properties which are editable in the material
        //_MainTex("Texture", 2D) = "red" {}
        _WireframeShader_Thickness("Thickness", Range(0, 1)) = 0.02 
		_WireframeShader_Smoothness("Smoothness", Range(0, 1)) = 0.01	
		_WireframeShader_Diameter("Diameter", Range(0, 1)) = 0.01
    }
    SubShader{
        Tags { "RenderType" = "Opaque" "RenderPipeline" = "UniversalPipeline" }
        LOD 100
        // Forward Lit Pass. The main pass which renders colors
        Pass {
            Lighting On
            Name "ForwardLit"
            Tags { "LightMode" = "UniversalForward" }
            
            //CGPROGRAM
            //ENDCG
            
            HLSLPROGRAM
            // Signal this shader requires compute buffers
            #pragma prefer_hlslcc gles
            #pragma exclude_renderers d3d11_9x
            #pragma target 5.0

            // Register our functions
            #pragma vertex Vertex
            #pragma fragment Fragment
                               
            #include "UnityCG.cginc"            
            #include "WireFrameGPU.cginc"
            
            float _WireframeShader_Thickness;
            float _WireframeShader_Smoothness;
            float _WireframeShader_Diameter;
            
            struct DrawVertex {
                float4 positionWS; // position in world space                
                float3 uv3; // used for wireframe data                
            };

            struct DrawTriangle {
                //float3 normalWS; // normal in world space. All points share this normal
                DrawVertex vertices[3];
            };

            StructuredBuffer<DrawTriangle> _DrawTriangles;

            struct VertexOutput {
                float3 uv3            : TEXCOORD0;   // Wireframe data is saved inside uv4 buffer (note, in shaders uv4 is read using TEXCOORD3)                
                float4 positionCS     : SV_POSITION; // Position in clip space
            };

            VertexOutput Vertex(uint vertexID: SV_VertexID) {

                VertexOutput output = (VertexOutput)0;
                DrawTriangle tri = _DrawTriangles[vertexID / 3];
                DrawVertex input = tri.vertices[vertexID % 3];                
                
                output.uv3 = input.uv3.xyz;
                output.positionCS = UnityObjectToClipPos(input.positionWS);                
                return output;
            }

            float4 Fragment(VertexOutput input) : SV_Target {    
                return ReadTriangleMassFromUV(input.uv3, _WireframeShader_Thickness, _WireframeShader_Smoothness, _WireframeShader_Diameter);    
            }

            ENDHLSL
            
        }
    }
}