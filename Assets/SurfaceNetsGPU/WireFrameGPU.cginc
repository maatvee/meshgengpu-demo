#ifndef WIREFRAME_GPU_CGINC
#define WIREFRAME_GPU_CGINC

float ReadTriangleMassFromUV(float3 uv, float thickness, float smoothness, float diameter) {
    float3 fw = abs(ddx(uv)) + abs(ddy(uv));
    float3 s = fw * (1 + smoothness * 10) + 1e-6;
    float3 l = uv - thickness.xxx;

    float3 a1 = smoothstep(0, s, l);	
    float w1 = min(min(a1.x, a1.y), a1.z);	
	
    float3 a2 = l / s;
    float w2 = smoothstep(0.0, 1.0, min(a2.x, min(a2.y, a2.z)) + 0.5);
    return 1 - saturate(lerp(w2, w1, diameter));
}

#endif