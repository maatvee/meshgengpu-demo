using System;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace SurfaceNetsGPU {
    public class MeshUtilsGPU : MonoBehaviour {

        /* 
         * Set up the 8 corners of each voxel like this:
         * Very important that this is set up the same way as the cube edges
         * 
         *  y         z
         *  ^        /     
         *  |
         *    6----7
         *   /|   /|
         *  4----5 |
         *  | 2--|-3
         *  |/   |/
         *  0----1   --> x
         * 
         */

        public static Vector3[] voxelCornerOffsets = new Vector3[8] {
            new Vector3(0f, 0f, 0f), // 0
            new Vector3(1f, 0f, 0f), // 1
            new Vector3(0f, 1f, 0f), // 2
            new Vector3(1f, 1f, 0f), // 3
            new Vector3(0f, 0f, 1f), // 4
            new Vector3(1f, 0f, 1f), // 5
            new Vector3(0f, 1f, 1f), // 6
            new Vector3(1f, 1f, 1f) // 7
        };

        /*
         * convert coordinates from the voxel index to world space position
         * i.e. convert voxel[u,v,w] to a Vector3 with an x,y,z in our world space coordinate plane 
         */

        public Vector3 GetWorldSpaceSamplePosition(int x, int y, int z, Bounds sampleSpace, Vector3 sampleResolution) {
            return sampleSpace.min + new Vector3(
                x * sampleSpace.size.x / sampleResolution.x,
                y * sampleSpace.size.y / sampleResolution.y,
                z * sampleSpace.size.z / sampleResolution.z);
        }

        /*
         * Build an intersection table. This is a 2^(cube config) -> 2^(edge config) map
         * There is only one entry for each possible cube configuration
         * and the output is a 12-bit vector enumerating all edges
         * crossing the 0-level
         */


        public static int[] GenerateIntersectionTable(int[] cubeEdges) {
            var edges = new int[256];
            
            for (int i = 0; i < 256; ++i) {
                int em = 0;
                for (int j = 0; j < 24; j += 2) {
                    var a = Convert.ToBoolean(i & (1 << cubeEdges[j]));
                    var b = Convert.ToBoolean(i & (1 << cubeEdges[j + 1]));
                    em |= a != b ? (1 << (j >> 1)) : 0;
                }

                //edgeTable[i] = em;
                edges[i] = em;
            }

            return edges;
        }

        /* 
         * Utility function to build a table of possible edges for a cube with each
         * pair of points representing one edge i.e. [0,1,0,2,0,4,...] would be the 
         * edges from points 0 to 1, 0 to 2, and 0 to 4 respectively:
         * 
         *  y         z
         *  ^        /     
         *  |
         *    6----7
         *   /|   /|
         *  4----5 |
         *  | 2--|-3
         *  |/   |/
         *  0----1   --> x
         * 
         */

        public static int[] GenerateCubeEdgesTable() {
            var cubes = new int[24];
            
            int k = 0;
            for (int i = 0; i < 8; ++i) {
                for (int j = 1; j <= 4; j <<= 1) {
                    int p = i ^ j;
                    if (i <= p) {
                        // cubeEdges[k++] = i;
                        // cubeEdges[k++] = p;
                        
                        cubes[k++] = i;
                        cubes[k++] = p;
                    }
                }
            }

            return cubes;
        }

        /*
         CopyMesh();
         Creates a new copy of an array, not just a reference
         */
        private static Mesh CopyMesh(Mesh meshToCopy) {

            var tempMesh = new Mesh();

            tempMesh.vertices = meshToCopy.vertices;
            tempMesh.triangles = meshToCopy.triangles;
            tempMesh.uv = meshToCopy.uv;
            tempMesh.normals = meshToCopy.normals;
            tempMesh.colors = meshToCopy.colors;
            tempMesh.tangents = meshToCopy.tangents;

            return tempMesh;
        }
        
        // This applies the game object's transform to the local bounds
        // Code by benblo from https://answers.unity.com/questions/361275/cant-convert-bounds-from-world-coordinates-to-loca.html
        public Bounds TransformBounds(Bounds boundsOS) {
            var center = transform.TransformPoint(boundsOS.center);

            // transform the local extents' axes
            var extents = boundsOS.extents;
            var axisX = transform.TransformVector(extents.x, 0, 0);
            var axisY = transform.TransformVector(0, extents.y, 0);
            var axisZ = transform.TransformVector(0, 0, extents.z);

            // sum their absolute value to get the world extents
            extents.x = Mathf.Abs(axisX.x) + Mathf.Abs(axisY.x) + Mathf.Abs(axisZ.x);
            extents.y = Mathf.Abs(axisX.y) + Mathf.Abs(axisY.y) + Mathf.Abs(axisZ.y);
            extents.z = Mathf.Abs(axisX.z) + Mathf.Abs(axisY.z) + Mathf.Abs(axisZ.z);

            return new Bounds { center = center, extents = extents };
        }


    }
}