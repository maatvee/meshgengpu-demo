// Make sure this file is not included twice
#ifndef MESHUTILSGPU_INCLUDED
#define MESHUTILSGPU_INCLUDED

struct Voxel {
    int vertexIndex;
    float3 vertexPosition;
    int isOnSurface;
    int voxelEdgeMask;
    int cornerMask;
};

struct DrawVertex {
    float4 positionWS; // position in world space
    float3 uv3; // used for wireframe data
};

struct DrawTriangle {    
    DrawVertex vertices[3];
};

struct Triangle {
    float3 a;
    float3 b;
    float3 c;
};

struct Quad {
    float3 a;
    float3 b;
    float3 c;
    float3 d;    
};

struct SampleSpace {
    float3 position;
    float3 extents;
    float3 size;
};

struct Sample {
    float3 samplePoint;
    float value;
};

struct FieldObject {
    int type; // 0 - point, 1 sphere, 2 box, 3 spline
    float3 pos;
    float radius;
    float3 boxSize;
};

StructuredBuffer<int> _EdgeTable;
StructuredBuffer<int> _CubeEdges;
StructuredBuffer<float3> _VoxelCornerOffsets;
StructuredBuffer<FieldObject> _FieldObjects;
RWStructuredBuffer<Voxel> _VoxelDataBuffer;
AppendStructuredBuffer<DrawTriangle> _DrawTriangles;
AppendStructuredBuffer<float4> _DebugBuffer;

// uint _gridX;
// uint _gridY;
// uint _gridZ;

// uint width = _gridX;
// uint height = _gridY;
// uint depth = _gridZ;

uint _FieldObjectsLength;
float4x4 _LocalToWorld;
float4x4 _WorldToLocal;
float4x4 _FO_LocalToWorld;
float4x4 _FO_WorldToLocal;

float _SamplesPerMeter;

float _Threshold;

uint _inspectIndex;
bool _inspectIndexEnabled;
bool _subdivideMesh;

int inX;
int inY;
int inZ;

uint _gridX = 32;
uint _gridY = 32;
uint _gridZ = 32;

int _xPos;
int _yPos;
int _zPos;

bool ConvertToBoolean(int value) {    
    return value != 0;
}

float3 SampleSpaceMin(SampleSpace sampleSpace,float _SamplesPerMeter) {
    float minX = sampleSpace.position.x - sampleSpace.extents.x + _xPos;
    float minY = sampleSpace.position.y - sampleSpace.extents.y + _yPos;
    float minZ = sampleSpace.position.z - sampleSpace.extents.z + _zPos;
    
    // float minX = sampleSpace.position.x - sampleSpace.extents.x - (_gridX - _SamplesPerMeter * _gridX);
    // float minY = sampleSpace.position.y - sampleSpace.extents.y - (_gridY - _SamplesPerMeter * _gridY);
    // float minZ = sampleSpace.position.z - sampleSpace.extents.z - (_gridZ - _SamplesPerMeter * _gridZ);

    return float3(minX,minY,minZ);
};

float3 GetWorldSpaceSamplePosition(int x, int y, int z, SampleSpace sampleSpace, float3 sampleResolution) {
    float3 sampleMin = SampleSpaceMin(sampleSpace,_SamplesPerMeter);
    
    return sampleMin + float3(
        (x * sampleSpace.size.x / sampleResolution.x),
        (y * sampleSpace.size.y / sampleResolution.y),
        (z * sampleSpace.size.z / sampleResolution.z));
}

float3 TransformToWorldSpace(float3 sourceVector) {    
    return mul(_LocalToWorld, float4(sourceVector, 1)).xyz;    
}

float3 TransformToLocalSpace(float3 sourceVector) {    
    return mul(_WorldToLocal, float4(sourceVector, 1)).xyz;    
}

bool VecNotZero ( float4 a ) {
    return (a.x != 0) && (a.y != 0) && (a.z != 0);
}

float SphericalField(float3 samplePosition, float3 centerOfSphere, float sphereRadius) {        
    float3 s = mul(_FO_LocalToWorld, float4(samplePosition, 1)).xyz;
    float3 c = mul(_FO_LocalToWorld, float4(centerOfSphere, 1)).xyz;

    float result = distance(s, c) - sphereRadius;  
    return result;        
}

float SqrMagnitude(float3 vec) {
    return sqrt(dot(vec,vec));
}

// does not currently support rotated boxes, only axis aligned boxes,
// unless rotation is applied to the transform of the MeshGPU GameObject
// which is passed in as the Matrix4x4 _LocalToWorld
float BoxField(float3 samplePosition, float3 centerOfBox, float3 boxSize) {    
    float3 s = mul(_FO_LocalToWorld, float4(samplePosition, 1)).xyz;    
    s -= centerOfBox;

    float3 d = float3(abs(s.x), abs(s.y), abs(s.z)) - (boxSize * 0.5);
    float3 maxD = float3(max(d.x, 0.0), max(d.y, 0.0), max(d.z, 0.0));
    return max(max(d.x, max(d.y, d.z)), 0.0) + SqrMagnitude(maxD);
}

float SampleField(SampleSpace sampleSpace,float3 samplePosition, float threshold) {
        // set the default final result to infinity
        float infinity = 10000000;            
        float finalResult = infinity;        
    
        for (uint i = 0; i < _FieldObjectsLength; i++){

            // Point FieldObject
            if (_FieldObjects[i].type == 0) {
                finalResult = min(
                    finalResult,
                    SphericalField(
                    samplePosition,
                        _FieldObjects[i].pos,
                        _FieldObjects[i].radius + threshold
                                                    
                    )
                );
            // Box FieldObject  
            } else if (_FieldObjects[i].type == 2) {
                 
                finalResult = min(
                    finalResult,
                    BoxField(
                        samplePosition,
                        _FieldObjects[i].pos,
                        _FieldObjects[i].boxSize)
                );

            // Sphere FieldObject  
            } else if (_FieldObjects[i].type == 1) {
                finalResult = min(
                    finalResult,
                    SphericalField(
                    samplePosition,
                        _FieldObjects[i].pos,
                        _FieldObjects[i].radius + threshold                                                    
                    )
                );
            }
        }   
 
    return finalResult;
}

void WriteTriangle(DrawVertex a, DrawVertex b, DrawVertex c) {
    // Create a draw triangle from three points
    DrawTriangle tri;    
    tri.vertices[0] = a;
    tri.vertices[1] = b;
    tri.vertices[2] = c;
    
    _DrawTriangles.Append(tri);      
}

void CalculateVertexPositions(uint idx, SampleSpace sampleSpace,float3 sampleResolution,int x,int y,int z){
    Voxel vox;
    float3 vertPos = float3(0,0,0);
    // default values.
    vox.isOnSurface = 0;
    vox.voxelEdgeMask = 0;
    vox.vertexIndex = -1;
    vox.cornerMask = 0;
    vox.vertexPosition = vertPos;

    // here we populate the buffer with an empty voxel to start with
    // I want to see if this is more effective than a separate compute shader to clear the buffer each frame 
    _VoxelDataBuffer[idx] = vox;

    uint cornerMask = 0; 
    float threshold = _Threshold;
    // sample the 8 corners for the voxel and create a corner mask
    Sample samples[8];

    for (uint i = 0; i < 8; i++) {
        float3 offset = _VoxelCornerOffsets[i];
        float3 samplePos = GetWorldSpaceSamplePosition(x + (int) offset.x, y + (int) offset.y, z + (int) offset.z, sampleSpace, sampleResolution);
        float sample = SampleField(sampleSpace,samplePos,threshold);
        samples[i].value = sample;
        samples[i].samplePoint = samplePos;      
        cornerMask |= ((sample > threshold) ? (1 << i) : 0);
    };

    // get edgemask from table using our corner mask
    int edgeMask = _EdgeTable[cornerMask];
    int edgeCrossings = 0;    

    // Check for early termination if cell does not intersect boundary
    // this is probably redundant in a GPU context since there is no saving by returning early
    // it just means the current GPU thread becomes idle sooner I guess
    if(cornerMask == 0 || cornerMask == 0xff) {
        // here we use return the default voxel as the main function is no longer in a x,y,z loop
        // this voxel's vertex will be set to 0,0,0 which gets ignored later and no triangle will be generated for these voxels
        //return vox;

        return;
    };
        
    for(int em=0; em < 12; ++em) {

        //Use edge mask to check if it is crossed
        if( !( (edgeMask & (1<<em)) > 0 ) ) {
            continue;
        }

        //If it did, increment number of edge crossings
        ++edgeCrossings;

        //Now find the point of intersection
        int e0 = _CubeEdges[ em<<1 ];
        int e1 = _CubeEdges[(em<<1)+1];
        float g0 = samples[e0].value;
        float g1 = samples[e1].value;
        float t  = (threshold - g0 ) / (g1 - g0);

        vertPos += float3(lerp(samples[e0].samplePoint, samples[e1].samplePoint, t));
    }
    vertPos /= edgeCrossings;

    vox.vertexPosition = vertPos;    
    vox.isOnSurface = 1;
    vox.voxelEdgeMask = edgeMask;    
    vox.cornerMask = cornerMask;
    vox.vertexIndex = idx;

    _VoxelDataBuffer[idx] = vox;
}

float3 midpointLine(float3 a, float3 b){
    float midX = (a.x + b.x) * 0.5;
    float midY = (a.y + b.y) * 0.5;
    float midZ = (a.z + b.z) * 0.5;

    return float3(midX, midY, midZ);
};

float3 averagePoints(float3 top, float3 left, float3 right, float3 bottom ){
    float avX = (top.x + left.x + right.x + bottom.x) * 0.25;
    float avY = (top.y + left.y + right.y + bottom.y) * 0.25;
    float avZ = (top.z + left.z + right.z + bottom.z) * 0.25;

    return float3(avX, avY, avZ);
};

float DistanceToEdge(float3 a, float3 b, float3 c) {
    return length(cross(a - b, a - c));
};

// WireFrame Quad Generation - Based on two Triangles using the Quad Vertex Layout below
// here we generate barycentric coordinates to calculate triangle masses to determine which quad edges to render and which ones to ignore.
// unfortunately it's not perfect, so some quad diagnals get rendered, that really shouldn't
void CalculateTriangleMass(int tri, float3 vertex1, float3 vertex2, float3 vertex3, out float3 mass1, out float3 mass2, out float3 mass3) {
    
    float d1 = distance(vertex1, vertex2);
    float d2 = distance(vertex2, vertex3);
    float d3 = distance(vertex3,  vertex1);		

    float4 b = float4(0, 
        DistanceToEdge(vertex3, vertex1, vertex2) / d1, 
        DistanceToEdge(vertex1, vertex2, vertex3) / d2, 
        DistanceToEdge(vertex2, vertex1, vertex3) / d3);
    b /= min(b.y, min(b.z, b.w));

    mass1 = b.xzx;
    mass2 = b.xxw;
    mass3 = b.yxx;

    int edgeMass = 10000;
			
    // mass1.x = ((d1 > d2) && (d1 > d3)) ? edgeMass : 0;
    // mass1.z = ((d3 >= d1) && (d3 > d2)) ? edgeMass : 0;
    // mass2.y = ((d2 >= d1) && (d2 >= d3)) ? edgeMass : 0;

    // for a regular square Quad, the diagonal is usually the longest edge, and the edge you don't want to render
    // tri 0 is the first triangle in the Quad - ACD, AC is the diagonal edge and we don't want to draw this 
    // tri 1 is the second triangle in the Quad - ABC, CA is the diagonal edge and we don't want to draw this either

    if (tri == 1) {
        mass1.x = 0;
        mass1.z = edgeMass;
        mass2.y = 0;
    } else {
        mass1.x = edgeMass;
        mass1.z = 0;
        mass2.y = 0;
    }
}

/*
Quad Vertex Layout

A ------- B
|\        |
| \       |
|  \   B  |
|   \     |
|    \    |
|  A  \   |
|      \  |
|       \ |
D ------- C
quad vertex position
Tri A = ACD;
Tri B = ABC;
 */

void ComputeTriangles(float3 vecA, float3 vecB, float3 vecC, float3 vecD, float3 vecE, float3 vecF) {
    DrawVertex triA, triB ,triC ,triD, triE, triF;
    
    float w = 0;
    triA.positionWS = float4(vecA,w);   
    triB.positionWS = float4(vecB,w);    
    triC.positionWS = float4(vecC,w); 
    
    triD.positionWS = float4(vecD,w); 
    triE.positionWS = float4(vecE,w);
    triF.positionWS = float4(vecF,w);

    float3 massA, massB, massC, massD, massE, massF;

    // calc wireframe data for first triangle ABC
    CalculateTriangleMass(0,triA.positionWS, triB.positionWS, triC.positionWS, massA, massB, massC);

    // calc wireframe data for second triangle DEF
    CalculateTriangleMass(1,triD.positionWS, triE.positionWS, triF.positionWS, massD, massE, massF);
    
    // First Triangle
    triA.uv3.xyz = massA;
    triB.uv3.xyz = massB;
    triC.uv3.xyz = massC;

    // Second Triangle
    triD.uv3.xyz = massD;
    triE.uv3.xyz = massE;
    triF.uv3.xyz = massF;
    
    // add generated triangles to output buffer
    WriteTriangle(triA, triB, triC);
    WriteTriangle(triD, triE, triF);
}

void DrawQuad(Quad quad) {
    DrawVertex triA, triB ,triC ,triD, triE, triF;
    float3 vecA, vecB, vecC, vecD, vecE, vecF;
    
    vecA = quad.a;
    vecB = quad.c;
    vecC = quad.d;

    vecD = quad.a;
    vecE = quad.b;
    vecF = quad.c;
    
    float w = 0;

    triA.positionWS = float4(vecA,w);   
    triB.positionWS = float4(vecB,w);    
    triC.positionWS = float4(vecC,w); 
    
    triD.positionWS = float4(vecD,w); 
    triE.positionWS = float4(vecE,w);
    triF.positionWS = float4(vecF,w);

    float3 massA, massB, massC, massD, massE, massF;

    // calc wireframe data for first triangle ABC
    CalculateTriangleMass(0,triA.positionWS, triB.positionWS, triC.positionWS, massA, massB, massC);

    // calc wireframe data for second triangle DEF
    CalculateTriangleMass(1,triD.positionWS, triE.positionWS, triF.positionWS, massD, massE, massF);
    
    // First Triangle
    triA.uv3.xyz = massA;
    triB.uv3.xyz = massB;
    triC.uv3.xyz = massC;
    
    // Second Triangle
    triD.uv3.xyz = massD;
    triE.uv3.xyz = massE;
    triF.uv3.xyz = massF;
    
    // add generated triangles to output buffer
    WriteTriangle(triA, triB, triC);
    WriteTriangle(triD, triE, triF);
}

void DrawSubdividedQuad (Quad quad){

    float3 midTop = midpointLine(quad.a,quad.b);            //  A-B
    float3 midLeft = midpointLine(quad.a,quad.d);        //  A-D
    float3 midRight = midpointLine(quad.b,quad.c);    //  B-C
    float3 midBottom = midpointLine(quad.d,quad.c);   //  D-C

    float3 centerPoint = averagePoints(midTop,midLeft,midRight,midBottom);

    // output triangles
    Quad topLeft, topRight, bottomLeft, bottomRight;
    
    // topLeft    
    topLeft.a = quad.a;
    topLeft.b = midTop;
    topLeft.c = centerPoint;
    topLeft.d = midLeft;    
       
    // topRight    
    topRight.a = midTop;
    topRight.b = quad.b;
    topRight.c = midRight;
    topRight.d = centerPoint;       
    
    // bottomLeft
    bottomLeft.a = midLeft;
    bottomLeft.b = centerPoint;
    bottomLeft.c = midBottom;
    bottomLeft.d = quad.d;
            
    // bottomRight    
    bottomRight.a = centerPoint;
    bottomRight.b = midRight;
    bottomRight.c = quad.c;
    bottomRight.d = midBottom;

    DrawQuad(topLeft);
    DrawQuad(topRight);
    DrawQuad(bottomLeft);
    DrawQuad(bottomRight);
};

void ComputeMesh(uint idx, uint x, uint y, uint z, Voxel vox, uint width, uint height, uint depth) {
  
    int R[3];
  
    R[0] = 1;
    R[1] = width; // 8
    R[2] = width * height; // 7*7 = 64

    int mask = vox.cornerMask;

    // get edge mask
    int edgeMask = _EdgeTable[mask];

    //Add Faces (Loop Over 3 Base Components)
    for (uint bc = 0; bc < 3; bc++) {
        //First 3 Entries Indicate Crossings on Edge
        if (!ConvertToBoolean(edgeMask & (1 << bc))) {
            continue;
        }
        
        // Skip if on Boundary in terms of x y z
        if ( (bc == 0 && (y == 0 || z == 0)) ||
             (bc == 1 && (z == 0 || x == 0)) ||
             (bc == 2 && (x == 0 || y == 0))
            ) {
            continue;
        }

        //i - Axes, iu, iv - Ortho Axes
        int iu = (bc + 1) % 3;
        int iv = (bc + 2) % 3;

        //Otherwise, Look Up Adjacent Edges in Buffer
        int du = R[iu];
        int dv = R[iv];
        
        int pointA, pointC, pointD, pointB;
        
        Quad quadA, quadB;

        /*
        Quad Vertex Layout
        
        A ------- B
        |\        |
        | \       |
        |  \   B  |
        |   \     |
        |    \    |
        |  A  \   |
        |      \  |
        |       \ |
        D ------- C
        quad vertex position
        Tri A = ACD;
        Tri B = ABC;
        */
        
        //Flip Orientation Depending on Corner Sign        
        if (ConvertToBoolean(mask & 1)) {

            // Corner Sign is True or Positive
            // _VoxelDataBuffer Index Offsets
            pointA = idx;
            pointB = idx - dv;
            pointC = idx - du - dv;
            pointD = idx - du;          
            
            quadA.a = _VoxelDataBuffer[pointA].vertexPosition;
            quadA.b = _VoxelDataBuffer[pointB].vertexPosition;
            quadA.c = _VoxelDataBuffer[pointC].vertexPosition;                                     
            quadA.d = _VoxelDataBuffer[pointD].vertexPosition;
            
            // here we only create triangles for the given index (if enabled)
            if (_inspectIndexEnabled && idx == _inspectIndex && _subdivideMesh) {                
                DrawSubdividedQuad(quadA);                 
            } else if (!_inspectIndexEnabled && _subdivideMesh) {                                                
                DrawSubdividedQuad(quadA);
            } else {
                DrawQuad(quadA);  
            }
                    
        } else {
            // Corner Sign is False or Negative
            // _VoxelDataBuffer Index Offsets
            pointA = idx;
            pointB = idx - du;
            pointC = idx - du - dv;
            pointD = idx - dv;

            quadB.a = _VoxelDataBuffer[pointA].vertexPosition;
            quadB.b = _VoxelDataBuffer[pointB].vertexPosition;
            quadB.c = _VoxelDataBuffer[pointC].vertexPosition;                                     
            quadB.d = _VoxelDataBuffer[pointD].vertexPosition;
           
            // here we only create triangles for the given index (if enabled)
            if (_inspectIndexEnabled && idx == _inspectIndex && _subdivideMesh) {
                DrawSubdividedQuad(quadB);                
            } else if (!_inspectIndexEnabled && _subdivideMesh) {                
                DrawSubdividedQuad(quadB);
            } else {
                DrawQuad(quadB);  
            }
        }        
    }
}

#endif